package com.appmanager.api.users.ui.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appmanager.api.users.data.UserEntity;
import com.appmanager.api.users.service.UsersService;
import com.appmanager.api.users.shared.UserDto;
import com.appmanager.api.users.ui.model.request.CreateUserRequestModel;
import com.appmanager.api.users.ui.model.response.CreateUserResponseModel;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private UsersService usersService;
	
	@GetMapping("/status/verify")
	public String status(HttpServletRequest request) {
		String text1 = "Funcionando na porta " + env.getProperty("local.server.port") + ".";
		String text2 = "\n Com token = " + env.getProperty("token.secret");
		String text3 = "\n Ip = " + request.getRemoteAddr();
		return text1 + text2 + text3;
	}
	
	@PostMapping(
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
			)
	public ResponseEntity<CreateUserResponseModel> createUser(@Valid @RequestBody CreateUserRequestModel createUserRequestModel) {
		
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		
		UserDto userDto = modelMapper.map(createUserRequestModel, UserDto.class);
		UserDto createUserDto = this.usersService.createUser(userDto);
		
		CreateUserResponseModel response = modelMapper.map(createUserDto, CreateUserResponseModel.class);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
}
