package com.appmanager.api.users.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.appmanager.api.users.shared.UserDto;

public interface UsersService extends UserDetailsService {
	UserDto createUser(UserDto userDto);
	UserDto getUserDetailsByEmail(String email);
}
